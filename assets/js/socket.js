// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/web/endpoint.ex":
import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {token: window.userToken}})

// When you connect, you'll often need to authenticate the client.
// For example, imagine you have an authentication plug, `MyAuth`,
// which authenticates the session and assigns a `:current_user`.
// If the current user exists you can assign the user's token in
// the connection for use in the layout.
//
// In your "lib/web/router.ex":
//
//     pipeline :browser do
//       ...
//       plug MyAuth
//       plug :put_user_token
//     end
//
//     defp put_user_token(conn, _) do
//       if current_user = conn.assigns[:current_user] do
//         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
//         assign(conn, :user_token, token)
//       else
//         conn
//       end
//     end
//
// Now you need to pass this token to JavaScript. You can do so
// inside a script tag in "lib/web/templates/layout/app.html.eex":
//
//     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
//
// You will need to verify the user token in the "connect/2" function
// in "lib/web/channels/user_socket.ex":
//
//     def connect(%{"token" => token}, socket) do
//       # max_age: 1209600 is equivalent to two weeks in seconds
//       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
//         {:ok, user_id} ->
//           {:ok, assign(socket, :user, user_id)}
//         {:error, reason} ->
//           :error
//       end
//     end
//
// Finally, pass the token on connect as below. Or remove it
// from connect if you don't care about authentication.

socket.connect()

// Now that you are connected, you can join channels with a topic:
let channel = socket.channel("room:lobby", {})
let chatInput         = document.querySelector("#chat-input")
let messagesContainer = document.querySelector("#messages")

chatInput.addEventListener("keypress", event => {
  if(event.keyCode === 13){
    event.preventDefault();
    
    channel.push("new_msg", {body: chatInput.innerHTML})
    checkAddNote(chatInput.innerHTML);

    chatInput.innerHTML = ""
  }
})


channel.on("new_msg", payload => {
  console.log(payload);
  let messageItem = document.createElement("li");
  messageItem.innerText = `${payload.body}`
  messagesContainer.appendChild(messageItem)
})

channel.on("note_move", payload => {
  console.log(payload);
  move_note(payload['note_event'][0], payload['note_event'][1], payload['note_event'][2], payload['note_event'][3]);
})

channel.on("note_create", payload => {
  console.log(payload);
  addNote(payload['note_event'][0], payload['note_event'][1], payload['note_event'][2], payload['note_event'][3]);
})

function sendNote(note_id, newX, newY, text) {
  channel.push("note_move", {note_event: [note_id, newX, newY, text]});
}

function createNewNote(note_id, newX, newY, text) {
  channel.push("note_create", {note_event: [note_id, newX, newY, text]});
}

function checkAddNote(text) {
  if (text.includes('@addnote')) {
    createNewNote(-1,20,20,text.replace("@addnote", ""));
    return;
  }
}


function move_note(client_id, newX, newY, text) {
  let note = document.getElementById("postIt" + client_id.toString());
  note.style.left = newX.toString() + "px";
  note.style.top = newY.toString() + "px";
  document.getElementById("postIt" + client_id.toString() + "Text").innerHTML = text;
}


function addNote(client_id, posX, posY, text) {
  var div = document.createElement("div");
  let idName = 'postIt' + client_id.toString();
  div.id = idName;
  div.name = client_id.toString();
  div.class = 'postIt';
  div.innerHTML = '<div class=topBar></div><div class=form><div id="' + idName + 'Text" contenteditable class=textAria>' + text + '</div></div>';
  let board = document.getElementById('board');
  board.appendChild(div);
  div.style.position = 'absolute';

  div.style.left = posX.toString() + "px";
  div.style.top = posY.toString() + "px";

  $(function(){
    $("#" + idName).draggable({
        handle:  '.topBar',
        stop: function( event, ui ) {
          sendNote(this.name, this.getBoundingClientRect().left, this.getBoundingClientRect().top, this.children[1].children[0].innerHTML)
          console.log(this.getBoundingClientRect());
        }
      }); 
  });
}


channel.join()
  .receive("ok", resp => { 
    for (let message of resp["room"]) {
      let messageItem = document.createElement("li");
      messageItem.innerText = message
      messagesContainer.appendChild(messageItem)
    }

    for (let note of resp["notes"]) {
      addNote(note.client_id,note.posX,note.posY, note.text);
    }

    document.getElementById('createNoteButton').onclick = function() {
      createNewNote(-1, 20,20, "New Note");
    }

    console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

export default socket
