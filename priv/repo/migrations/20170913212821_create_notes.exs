defmodule Hello.Repo.Migrations.CreateNotes do
  use Ecto.Migration

  def change do
    create table(:notes) do
      add :text, :string
      add :posX, :integer
      add :posY, :integer

      timestamps()
    end

  end
end
