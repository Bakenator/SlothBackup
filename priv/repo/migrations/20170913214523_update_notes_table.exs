defmodule Hello.Repo.Migrations.UpdateNotesTable do
  use Ecto.Migration

  def change do
    alter table(:notes) do
      add :client_id, :integer
    end
  end
end
