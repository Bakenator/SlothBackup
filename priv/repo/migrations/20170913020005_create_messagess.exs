defmodule Hello.Repo.Migrations.CreateMessagess do
  use Ecto.Migration

  def change do
    create table(:messagess) do
      add :content, :string

      timestamps()
    end

  end
end
