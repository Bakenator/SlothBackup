defmodule HelloWeb.RoomChannel do
use Phoenix.Channel
alias Hello.Message
alias Hello.Boards.Note
require IEx

  intercept(["new_msg"])

  def join("room:lobby", _message, socket) do
    messages = Enum.map(Hello.Repo.all(Hello.Message), fn(x) -> x.content end)
    # notes = 1
    all_notes = Hello.Repo.all(Note)

    message_ids =
      all_notes
      |> Enum.map(fn(x) -> x.client_id end)
      |> Enum.uniq

   
    notes = 
      message_ids
      |> Enum.map(fn(x) -> Note.most_recent(x) end)

  
    {:ok, %{room: messages, notes: notes}, socket}
  end


  def join("room:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in("new_msg", %{"body" => body}, socket) do
    broadcast! socket, "new_msg", %{body: body}
    changeset = Message.changeset(%Message{}, %{content: body})
    {:ok, message} = Hello.Repo.insert(changeset)
    
    {:noreply, socket}
  end

  def handle_out("new_msg", payload, socket) do
    push socket, "new_msg", payload
    {:noreply, socket}
  end

  def handle_in("note_move", %{"note_event" => [note_id, newX, newY, text]}, socket) do
    new_note_id = Note.process_new_id(note_id)

    broadcast! socket, "note_move", %{note_event: [new_note_id, newX, newY, text]}

    changeset = Note.changeset(%Note{}, %{client_id: new_note_id, posX: newX, posY: newY, text: text})
    Hello.Repo.insert(changeset)
    
    {:noreply, socket}
  end

  def handle_in("note_create", %{"note_event" => [note_id, newX, newY, text]}, socket) do
    new_note_id = Note.process_new_id(note_id)

    changeset = Note.changeset(%Note{}, %{client_id: new_note_id, posX: 10, posY: 10, text: text})
    Hello.Repo.insert(changeset)

    broadcast! socket, "note_create", %{note_event: [new_note_id, 10, 10, text]}

    {:noreply, socket}
  end

end