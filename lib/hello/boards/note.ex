defmodule Hello.Boards.Note do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Hello.Boards.Note


  schema "notes" do
    field :posX, :integer
    field :posY, :integer
    field :text, :string
    field :client_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Note{} = note, attrs) do
    note
    |> cast(attrs, [:text, :posX, :posY, :client_id])
    |> validate_required([:text, :posX, :posY, :client_id])
  end

  def most_recent(client_id) do
    query = from q in Note, where: q.client_id == ^client_id, order_by: q.inserted_at
    Hello.Repo.all(query)
    |> Enum.reverse
    |> List.first
    |> (&(%{client_id: &1.client_id, posX: &1.posX, posY: &1.posY, text: &1.text})).()
  end

  def next_client_id() do
    Hello.Repo.all(Note)
    |> Enum.map(fn(x) -> x.client_id end)
    |> (&(&1 ++ [0])).()
    |> Enum.max
    |> (&(&1 + 1)).()
  end

  def process_new_id(note_id) when note_id < 0 do
    next_client_id
  end

  def process_new_id(note_id) do
    note_id
  end
end
